package com.example.plugins.tutorial.confluence.hipchat.notification;

import com.atlassian.confluence.event.events.content.comment.CommentCreateEvent;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.plugins.hipchat.spacetoroom.api.notifications.SpaceToRoomNotification;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.api.icons.ADGIcon;
import com.atlassian.hipchat.api.icons.Icon;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.user.UserKey;

import static java.lang.String.format;

/**
 * The is an example HipChat space to room notification the notifies a room
 * when a space is updated.
 */
public class ExampleNotification extends SpaceToRoomNotification<CommentCreateEvent> {
    private ApplicationProperties applicationProperties;
    private I18NBeanFactory i18NBeanFactory;

    public ExampleNotification() {
    }

    @Override
    public Icon getIcon(CommentCreateEvent event) {
        return ADGIcon.PAGE;
    }

    @Override
    public Option<UserKey> getUser(CommentCreateEvent event) {
        ConfluenceUser creator = event.getComment().getCreator();

        if (creator != null) {
            return Option.some(creator.getKey());
        }
        return Option.none();
    }

    @Override
    public String getLink(CommentCreateEvent event) {
        Comment comment = event.getComment();
        String text = i18NBeanFactory.getI18NBean().getText("hipchat.notification.comment.link.text");
        return format("<a href=\"%s\"><b>%s</b></a>",
            GeneralUtil.escapeForHtmlAttribute(linkUrl(comment)),
            GeneralUtil.escapeXMLCharacters(text));
    }

    private String linkUrl(Comment comment) {
        return applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + comment.getUrlPath();
    }

    @Override
    public String getMessageKey(CommentCreateEvent event) {
        return "hipchat.notification.message.comment.added";
    }

    @Override
    public Option<Space> getSpace(CommentCreateEvent event) {
        return Option.some(event.getComment().getSpace());
    }

    @Override
    public Class<CommentCreateEvent> getEventClass() {
        return CommentCreateEvent.class;
    }

    @Override
    public boolean shouldSend(CommentCreateEvent event) {
        return true;
    }

    public void setApplicationProperties(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public void setI18NBeanFactory(I18NBeanFactory i18NBeanFactory) {
        this.i18NBeanFactory = i18NBeanFactory;
    }
}
